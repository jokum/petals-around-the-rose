import java.util.Random;

/**
 * This is a Petals Around the Rose model/controller.
 */
public class Patr
{
    public boolean hasRolled()
    {
        return diceRolled;
    }

    public int totalRolls()
    {
        return totalRolls;
    }

    public int getPetals()
    {
        return petals;
    }

    public int correctGuesses()
    {
        return correctGuesses;
    }

    public int streak()
    {
        return currentRun;
    }

    public int bestRun()
    {
        return bestRun;
    }

    /**
     * Performs a guess.
     * @param guess the guess to try.
     * @return true if the guess was correct, false otherwise.
     */
    public boolean guess(int guess)
    {
        if (guess == petals)
        {
            ++currentRun;
            ++correctGuesses;
            bestRun = currentRun >= bestRun ? currentRun : bestRun;
            diceRolled = false;
            return true;
        }
        currentRun = 0;
        return false;
    }

    public byte[] getDice()
    {
        return dieFaces;
    }

    /**
     * Perform a dice roll and calculate the correct answer.
     */
    public void roll()
    {
        diceRolled = true;
        petals = 0;
        ++totalRolls;

        for (byte i = 0; i < dieFaces.length; i++)
        {
            dieFaces[i] = (byte)(Math.floor(Math.random()*6)+1);

            if (dieFaces[i] == 3 || dieFaces[i] == 5)
            {
                petals += dieFaces[i] - 1;
            }
        }
    }

    private boolean diceRolled = false;
    private int totalRolls = 0,
            correctGuesses = 0,
            currentRun = 0,
            bestRun = 0,
            petals = 0;
    private byte[] dieFaces = new byte[5];
}
