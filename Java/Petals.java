import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Petals extends javax.swing.JFrame
{
    public Petals(Patr controller)
    {
        this.controller = controller;
        initComponents();
    }

    public static void main(String args[])
    {
        new Petals(new Patr()).setVisible(true);
    }

    // Initialize components.
    private void initComponents()
    {
        textAreaInstructions = new JTextArea();
        dlgInstructions = new JDialog();
        pnlInstructions = new JPanel();
        pnlCtrl = new JPanel();
        pnlDice = new JPanel();
        dice = new JLabel[5];
        txtCorrectGuesses = new JTextField();
        txtTotalRolls = new JTextField();
        txtBestRun = new JTextField();
        txtGuess = new JTextField();
        lblCorrectGuesses = new JLabel();
        lblCurrentGuess = new JLabel();
        lblTotalRolls = new JLabel();
        lblBestRun = new JLabel();
        btnInstructions = new JButton();
        btnRollDice = new JButton();
        btnGuess = new JButton();
        btnExit = new JButton();
        btnOK = new JButton();

        for (int i = 0; i < dieGif.length; ++i)
        {
            dieGif[i] = new ImageIcon("dice/" + (i + 1) + ".gif");
        }

        for (int i = 0; i < dice.length; ++i)
        {
            dice[i] = new JLabel();
            dice[i].setIcon(dieGif[0]);
        }

        lblCorrectGuesses.setText("Correct Guesses:");
        lblCurrentGuess.setText("Petals Around the Rose:");
        lblTotalRolls.setText("Total Rolls:");
        lblBestRun.setText("Best Run:");

        btnInstructions.setText("Instructions");
        btnRollDice.setText("Roll Dice");
        btnGuess.setText("Guess");
        btnExit.setText("Exit");
        btnOK.setText("OK");

        btnRollDice.addActionListener(new RollDiceAL());
        btnInstructions.addActionListener(new InstructionsAL());
        btnGuess.addActionListener(new GuessAL());
        btnExit.addActionListener(new ExitAL());
        btnOK.addActionListener(new OKAL());
        txtGuess.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                txtGuessMouseClicked();
            }
        });

        dlgInstructions.setResizable(false);
        pnlInstructions.setBorder(
                BorderFactory.createTitledBorder("Instructions"));
        textAreaInstructions.setEditable(false);
        textAreaInstructions.setLineWrap(true);
        textAreaInstructions.setText(
                "The purpose of this game is to guess the answer to each roll" +
                " of the dice knowing only:" +
                "\n\n     \u2022 That the name of the game is Petals Around" +
                " the Rose,\n           and that name is significant.\n" +
                "     \u2022 That the answer is always even.\n" +
                "     \u2022 The answer to any previous dice roll." +
                "\n\nTo play, click the Roll Dice button, input you guess" +
                " numerically (integers, no letters) in the top-most text" +
                " field, and click the Guess button. The computer will then" +
                " calculate the correct answer, compare it to your guess, and" +
                " inform you whether you guessed correctly." +
                "\n\nTo get the answer to any given roll, simply guess. If" +
                " your guess is incorrect, the computer will reveal the" +
                " answer." +
                "\n\nYou must roll the dice after each guess.");
        textAreaInstructions.setWrapStyleWord(true);
        textAreaInstructions.setBorder(BorderFactory.createEtchedBorder());

        // Instruction window.
        GroupLayout pnlInstructionsLayout = new GroupLayout(pnlInstructions);
        pnlInstructions.setLayout(pnlInstructionsLayout);
        pnlInstructionsLayout.setHorizontalGroup(
                pnlInstructionsLayout.createParallelGroup(
                    GroupLayout.Alignment.LEADING)
                .addGroup(pnlInstructionsLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(textAreaInstructions, 360, 360, 360)
                    .addContainerGap()));
        pnlInstructionsLayout.setVerticalGroup(
                pnlInstructionsLayout.createParallelGroup(
                    GroupLayout.Alignment.LEADING)
                .addGroup(pnlInstructionsLayout.createSequentialGroup()
                    .addComponent(textAreaInstructions, 295, 295, 295)
                    .addContainerGap()));

        GroupLayout dlgInstructionsLayout = 
            new GroupLayout(dlgInstructions.getContentPane());
        dlgInstructions.getContentPane().setLayout(dlgInstructionsLayout);
        dlgInstructionsLayout.setHorizontalGroup(
                dlgInstructionsLayout.createParallelGroup()
                .addGroup(dlgInstructionsLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlInstructions)
                    .addContainerGap())
                .addGroup(dlgInstructionsLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnOK, 80, 80, 80)));

        dlgInstructionsLayout.setVerticalGroup(
                dlgInstructionsLayout.createParallelGroup()
                .addGroup(dlgInstructionsLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlInstructions)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnOK)
                    .addContainerGap()));
        dlgInstructions.pack();

        // The five dice.
        pnlDice.setBorder(BorderFactory.createEtchedBorder());

        GroupLayout pnlDiceLayout = new GroupLayout(pnlDice);
        GroupLayout.SequentialGroup sg = pnlDiceLayout.createSequentialGroup();
        GroupLayout.ParallelGroup pg = pnlDiceLayout.createParallelGroup();

        sg.addContainerGap();
        for (JLabel lbl : dice)
        {
            sg.addComponent(lbl);
            pg.addComponent(lbl);
        }
        sg.addContainerGap();
        pnlDice.setLayout(pnlDiceLayout);
        pnlDiceLayout.setHorizontalGroup(sg);

        sg = pnlDiceLayout.createSequentialGroup();
        sg.addContainerGap().addGroup(pg).addContainerGap();
        pnlDiceLayout.setVerticalGroup(sg);

        // The controls.
        pnlCtrl.setBorder(BorderFactory.createEtchedBorder());
        GroupLayout pnlCtrlLayout = new GroupLayout(pnlCtrl);

        sg = pnlCtrlLayout.createSequentialGroup()
            .addGroup(pnlCtrlLayout.createParallelGroup()
                    .addGroup(pnlCtrlLayout.createSequentialGroup()
                        .addComponent(btnRollDice)
                        .addComponent(btnGuess))
                    .addComponent(btnInstructions))
            .addGroup(pnlCtrlLayout.createParallelGroup(
                        GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTotalRolls)
                    .addComponent(lblBestRun)
                    .addComponent(lblCorrectGuesses));

        // Wrap above sg in below sg.
        sg = pnlCtrlLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlCtrlLayout.createParallelGroup()
                    .addGroup(pnlCtrlLayout.createSequentialGroup()
                        .addComponent(lblCurrentGuess)
                        .addPreferredGap(
                            LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtGuess))
                    .addGroup(pnlCtrlLayout.createSequentialGroup()
                        .addGroup(sg)
                        .addPreferredGap(
                            LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlCtrlLayout.createParallelGroup(
                                GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotalRolls)
                            .addComponent(txtCorrectGuesses)
                            .addComponent(txtBestRun, 60, 60, 60))))
            .addContainerGap();

        pnlCtrl.setLayout(pnlCtrlLayout);
        pnlCtrlLayout.setHorizontalGroup(sg);

        pnlCtrlLayout.linkSize(SwingConstants.HORIZONTAL,
                new Component[] { btnGuess, btnInstructions, btnRollDice });

        pg = pnlCtrlLayout.createParallelGroup()
            .addGroup(pnlCtrlLayout.createSequentialGroup()
                    .addGroup(pnlCtrlLayout.createParallelGroup()
                        .addComponent(txtTotalRolls)
                        .addComponent(lblTotalRolls))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlCtrlLayout.createParallelGroup()
                        .addComponent(txtCorrectGuesses)
                        .addComponent(lblCorrectGuesses))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlCtrlLayout.createParallelGroup()
                        .addComponent(txtBestRun)
                        .addComponent(lblBestRun)))
            .addGroup(pnlCtrlLayout.createSequentialGroup()
                    .addGroup(pnlCtrlLayout.createParallelGroup()
                        .addComponent(btnGuess)
                        .addComponent(btnRollDice))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnInstructions));

        pnlCtrlLayout.setVerticalGroup(pnlCtrlLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCtrlLayout.createParallelGroup()
                    .addComponent(lblCurrentGuess)
                    .addComponent(txtGuess))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pg).addContainerGap());

        // Main frame.
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Petals Around the Rose");
        setResizable(false);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup()
                    .addComponent(pnlDice, GroupLayout.Alignment.CENTER)
                    .addComponent(pnlCtrl)
                    .addComponent(btnExit, GroupLayout.Alignment.TRAILING,
                        80, 80, 80))
                .addContainerGap());

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDice)
                .addContainerGap()
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlCtrl)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExit)
                .addContainerGap());
        pack();
    }

    private class ExitAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            btnExitClick();
        }
    }

    private class OKAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            btnOKClick();
        }
    }

    private class InstructionsAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            btnInstructionsClick();
        }
    }

    private class GuessAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            btnGuessClick();
        }
    }

    private class RollDiceAL implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            btnRollDiceClick();
        }
    }

    // ActionEvents.
    private void txtGuessMouseClicked()
    {
        txtGuess.setText("");
    }

    private void btnInstructionsClick()
    {
        dlgInstructions.setVisible(true);
    }

    private void btnOKClick()
    {
        dlgInstructions.setVisible(false);
    }

    private void btnExitClick()
    {
        System.exit(0);
    }

    private void btnRollDiceClick()
    {
        controller.roll();
        byte[] diceFaces = controller.getDice();

        for (int i = 0; i < diceFaces.length; i++)
        {
            dice[i].setIcon(dieGif[diceFaces[i] - 1]);
        }

        txtTotalRolls.setText("" + controller.totalRolls());
    }

    private void btnGuessClick()
    {
        if (!controller.hasRolled())
        {
            txtGuess.setText("Please roll the dice.");
        }
        else if (txtGuess.getText().equals(""))
        {
            txtGuess.setText("You must input a number.");
        }
        else
        {
            int guess = 0;
            try
            {
                guess = Integer.parseInt((txtGuess.getText()));
            }
            catch (NumberFormatException e)
            {
                txtGuess.setText("That doesn't look like a number.");
                return;
            }

            if (controller.guess(guess))
            {
                txtGuess.setText(":: Yes! ::");
                txtCorrectGuesses.setText("" + controller.correctGuesses());
            }
            else
            {
                if (guess % 2 == 0)
                {
                    txtGuess.setText("It's " + controller.getPetals() +
                            ", not " + guess + ".");
                }
                else
                {
                    txtGuess.setText("It's " + controller.getPetals() +
                            ", not " + guess + ". The score is always even.");
                }
            }

            if (controller.streak() == 5)
            {
                txtGuess.setText("5-streak! Did you get it?");
            }
            else if (controller.streak() == 10)
            {
                txtGuess.setText("10-streak! It looks like you got it.");
            }

            txtBestRun.setText("" + controller.bestRun());
        }
    }

    private ImageIcon[] dieGif = new ImageIcon[6];
    private Patr controller;
    private JButton btnExit,
            btnGuess,
            btnInstructions,
            btnOK,
            btnRollDice;
    private JDialog dlgInstructions;
    private JLabel[] dice;
    private JLabel lblBestRun,
            lblCorrectGuesses,
            lblCurrentGuess,
            lblTotalRolls;
    private JPanel pnlDice,
            pnlCtrl,
            pnlInstructions;
    private JTextArea textAreaInstructions;
    private JTextField txtBestRun,
            txtCorrectGuesses,
            txtGuess,
            txtTotalRolls;
}
