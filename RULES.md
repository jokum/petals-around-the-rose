# The Rules

**Spoiler alert:** This document contains the calculations for *Petals Around the Rose*; that is, it reveals the answer to the objective of the game. Once the player knows this, the game ceases to be fun and the player will be cheated of the delicious feeling of achievement upon figuring
out the secret themselves. If you're okay with this, read on. Otherwise, stop right now.

## What the players are told

The computer will tell you three things:

1. The name of the game is *Petals Around the Rose*, and that name is significant.
2. The answer is always even.
3. The correct answer to any given throw.

It is the player's objective to determine how the correct score is calculated. They may guess as many times as they want on as many throws as they want.

## The secret

The name of the game is a metaphor for a die face. The "rose" is the centre eye and the surrounding eyes are "petals." The answer to a throw is the sum of all petals. Specifically:

* 1 => 0
* 2 => 0
* 3 => 2
* 4 => 0
* 5 => 4
* 6 => 0

Only 3s and 5s have both stalks and petals. 1s have no petals and the remainder have no stalks.
