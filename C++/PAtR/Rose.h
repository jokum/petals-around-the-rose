#ifndef ROSE_H
#define ROSE_H

#ifndef CPP11_FEATURES_H
#include "Cpp11Features.h"
#endif

#include <random>
#include <ctime>
#include <array>
#include <stdexcept>

namespace patr
{
    static std::mt19937 rng(static_cast<unsigned long>(time(0)));
    static std::uniform_int_distribution<int> dist(1, 6);

    // Just for shits and giggles. It's not like overflow will ever matter.
    template <typename T> inline void nowrap_inc(T& value)
    {
        if (value < std::numeric_limits<T>::max()) { ++value; }
    }

    class Rose
    {
    public:
        Rose(): dice(), throws(0), solution(0), guess_streak(0) { }

        void roll();
        unsigned operator[](unsigned i) const
        {
            if (i >= dice.size()) { throw std::out_of_range("there are only 6 dice."); }
            return dice[i];
        }

        int tell() const { return solution; }
        int size() const { return dice.size(); }
        int streak() const { return guess_streak; }
        bool guess(int val);

    private:
        std::array<int, 6> dice;
        int throws;
        int solution;
        int guess_streak;
    };
}
#endif // Include guard.