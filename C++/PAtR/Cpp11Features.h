#ifndef CPP11_FEATURES_H
#define CPP11_FEATURES_H

// for (T t : collection) { }
#if defined(_MSC_VER) && (_MSC_FULL_VER >= 170050727)
#define CPP11_FOR_EACH
#elif defined(__GNUC__) && (__GNUC__ >= 4) && (__GNUC_MINOR__ >= 6)
#define CPP11_FOR_EACH
#endif

#endif // Include guard.