#ifndef ROSE_H
#include "Rose.h"
#endif

namespace patr
{

    void Rose::roll()
    {
        nowrap_inc(throws);
        solution = 0;

#ifdef CPP11_FOR_EACH
        for (int &d : dice)
#else
        std::for_each(dice.begin(), dice.end(), [this](int &d)
#endif
        {
            d = patr::dist(rng);
            // If d is odd, increase solution by (d-1).
            solution += (d % 2) ? d - 1 : 0;
        }
#ifndef CPP11_FOR_EACH
        );
#endif
    }

    bool Rose::guess(int val)
    {
        if (val == solution)
        {
            ++guess_streak;
            return true;
        }
        else
        {
            guess_streak = 0;
            return false;
        }
    }
}