#ifndef PETALSCLI_H
#include "PetalsCLI.h"
#endif

#include <sstream>

using std::ends;
using std::endl;
using std::string;

namespace patr
{
    PetalsCLI::PetalsCLI(std::ostream &out, std::istream &in): os(out), is(in), rose(), dice_lines()
    {
        dice_lines.push_back("--------- --------- --------- --------- --------- ---------");
        dice_lines.push_back("|       | |       | |       | |       | |       | |       |");
        dice_lines.push_back("|       | |       | |       | |       | |       | |       |");
        dice_lines.push_back("|       | |       | |       | |       | |       | |       |");
        dice_lines.push_back("--------- --------- --------- --------- --------- ---------");
    }

    PetalsCLI& PetalsCLI::operator=(const PetalsCLI &rhs)
    {
        os.rdbuf(rhs.os.rdbuf());
        is.rdbuf(rhs.is.rdbuf());
        rose = rhs.rose;
        dice_lines = rhs.dice_lines;
        return *this;
    }

    void PetalsCLI::play()
    {
        std::string s;
        int n = 0;

        rose.roll();

        os << "Welcome to Petals Around the Rose!" << endl;
        print_help();

        for (;;)
        {
            print_dice();
            os << ">" << ends;

            is >> s;
            is.clear();
            is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            str_tolower(s);

            if (s == "t" || s == "tell")
            {
                os << "The answer is " << rose.tell();
                rose.roll();
            }
            else if (s == "h" || s == "help")
            {
                print_help();
            }
            else if (s == "q" || s == "quit" || s == "exit")
            {
                os << "Bye!";
                break;
            }
            else if (std::stringstream(s) >> n)
            {
                if (n % 2)
                {
                    os << "That's not right. The answer is always an even number.";
                }
                else if (rose.guess(n))
                {
                    os << "That's right!";
                    if (rose.streak() == 5)
                    {
                        os << " 5-streak -- did you figure it out?";
                    }
                    else if (rose.streak() == 10)
                    {
                        os << " 10-streak! I think you got it!";
                    }
                    rose.roll();
                }
                else
                {
                    os << "That's not right.";
                }
            }
            else
            {
                os << "Command unregocnized: " << s;
            }
            os << endl;
        }
    }

    void PetalsCLI::print_help()
    {
              //---------1---------2---------3---------4---------5---------6---------7---------80 characters.
        os  << "In Petals Around the Rose I will throw five dice, and it is your objective to" << endl
            << "figure out the result of any given throw. To this end I may tell you three" << endl
            << "things:" << endl
            << "\t1. The name of the game is Petals Around the Rose, and that name is" << endl
            << "\t   significant." << endl
            << "\t2. The answer to the current throw." << endl
            << "\t3. The fact that the result is always even." << endl << endl
            << "Commands:" << endl
            << "\t" << "any integer" << "\t"    << "Interpreted as a guess." << endl
            << "\t" << "t, tell" << "\t\t"      << "[T]ell the result." << endl
            << "\t" << "h, help" << "\t\t"      << "Show this screen." << endl
            << "\t" << "q, quit, exit" << "\t"  << "Exit." << endl
            << "All commands are case insensitive."
            << endl;
    }

    void PetalsCLI::print_dice()
    {
        for (int i = 0; i < rose.size(); ++i)
        {
            // false  true
            // o          o
            //   x      x
            //     o  o
            bool slope_up = bernoulli(rng);
            const int l = i * dice_width + offset_left,
                m = i * dice_width + offset_mid,
                r = i * dice_width + offset_right,
                line_top = (slope_up ? 3 : 1),
                line_bot = (slope_up ? 1 : 3);

            // 1, 3, 5
            // x   x
            //   o  
            // x   x
            dice_lines[2][m] = (rose[i] % 2) ? 'o' : ' ';

            // 2, 3, 4, 5, 6
            // o   x
            // x x x
            // x   o
            if (rose[i] > 1)
            {
                dice_lines[line_top][l] = 'o';

                dice_lines[line_bot][r] = 'o';
            }
            else
            {
                dice_lines[line_top][l] = ' ';

                dice_lines[line_bot][r] = ' ';
            }

            // 4, 5, 6
            // x   o
            // x x x
            // o   x
            if (rose[i] > 3)
            {
                dice_lines[line_top][r] = 'o';

                dice_lines[line_bot][l] = 'o';
            }
            else
            {
                dice_lines[line_top][r] = ' ';

                dice_lines[line_bot][l] = ' ';
            }

            // 6
            // x   x
            // o x o
            // x   x
            if (rose[i] == 6)
            {
                dice_lines[2][l] = 'o';
                dice_lines[2][r] = 'o';
            }
            else
            {
                dice_lines[2][l] = ' ';
                dice_lines[2][r] = ' ';
            }
        }

        for (auto beg = dice_lines.begin(), end = dice_lines.end(); beg != end; ++beg)
        {
            os << *beg << endl;
        }
    }

    void PetalsCLI::str_tolower(string &s) const
    {
#ifdef CPP11_FOR_EACH
        for (char &c : s)
#else
        std::for_each(s.begin(), s.end(), [](char &c)
#endif
        { c = static_cast<char>(tolower(c)); }
#ifndef CPP11_FOR_EACH
        );
#endif
    }
}