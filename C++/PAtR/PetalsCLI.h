#ifndef PETALSCLI_H
#define PETALSCLI_H

#ifndef CPP11_FEATURES_H
#include "Cpp11Features.h"
#endif

#ifndef ROSE_H
#include "Rose.h"
#endif

#include <iostream>
#include <string>
#include <vector>

namespace patr
{
    // Inexplicably, default 0.5 always seems to produce slope_up=true for eyes 2 and 3.
    static std::bernoulli_distribution bernoulli(0.4);

    class PetalsCLI
    {
    public:
        PetalsCLI(std::ostream &out = std::cout, std::istream &in = std::cin);
        PetalsCLI(const PetalsCLI &orig): os(orig.os), is(orig.is), rose(orig.rose), dice_lines(orig.dice_lines) { }
        PetalsCLI& operator=(const PetalsCLI &rhs);

        void play();

    private:
        void print_help();
        void print_dice();
        void str_tolower(std::string &s) const;

        std::ostream &os;
        std::istream &is;
        Rose rose;
        std::vector<std::string> dice_lines;
        static const int dice_width = 10, offset_left = 2, offset_mid = 4, offset_right = 6;
    };
}
#endif // Include guard.