﻿#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include "patr.h"

#ifdef _WIN32
#define TL 218
#define TR 191
#define BL 192
#define BR 217
#define VB 179
#define HB 196
#else
#define TL '-'
#define TR '-'
#define BL '-'
#define BR '-'
#define VB '|'
#define HB '-'
#endif

#define S_DIE_FORM      "%c %s %c "
#define S_DIE_EYE_LR    "o   o"
#define S_DIE_EYE_L     "o    "
#define S_DIE_EYE_M     "  o  "
#define S_DIE_EYE_R     "    o"
#define S_DIE_EYE_NULL  "     "

#define EYES(I) t->dice[(I)][0]
#define SLOPE_UP(I) t->dice[(I)][1]

char *read_line(char *buf, const size_t len, FILE *f)
{
    char *p;

    if ((p = fgets(buf, len, f)) != NULL)
    {
        size_t last = strlen(buf) - 1;
        if (buf[last] == '\n') {
            buf[last] = '\0';
        } else {
            fscanf(f, "%*[^\n]");   /* Match anything but throw it away. */
            fgetc(f);               /* Now throw away the newline. */
        }
    }
    return p;
}

void lowercase(char *str, const size_t len)
{
    unsigned int i = 0;

    for (i = 0; i < len; ++i)
    {
        str[i] = tolower(str[i]);
    }
    str[len] = '\0';
}

void throw_dice(struct Throw *t)
{
    int i = t->result = 0;
    for (i = 0; i < NUM_DICE; ++i)
    {
        t->dice[i][0] = (uint8_t) ((double) rand() / (RAND_MAX - 1) * 6 + 1);
        t->dice[i][1] = rand() % 2;
        t->result += (t->dice[i][0] % 2) ? t->dice[i][0] - 1 : 0;
    }

    ++t->total_throws;
}

void print_vertical(const uint8_t num, const uint8_t top)
{
    uint8_t i = 0;
    for (i = 0; i < num; ++i)
    {
        printf("%c%c%c%c%c%c%c%c%c ", (top ? TL : BL), HB, HB, HB, HB, HB, HB, HB, (top ? TR : BR));
    }
    printf("\n");
}

void print_middle(const struct Throw *t)
{
    uint8_t i = 0;
    for (i = 0; i < NUM_DICE; ++i)
    {
        printf(S_DIE_FORM, VB, (EYES(i) > 3) ? S_DIE_EYE_LR : (EYES(i) > 1)
            ? (SLOPE_UP(i) ? S_DIE_EYE_R : S_DIE_EYE_L)
            : S_DIE_EYE_NULL, VB);
    }
    printf("\n");
    for (i = 0; i < NUM_DICE; ++i)
    {
        printf(S_DIE_FORM, VB, (EYES(i) % 2 == 1)
            ? S_DIE_EYE_M : (EYES(i) == 6) ? S_DIE_EYE_LR : S_DIE_EYE_NULL, VB);
    }
    printf("\n");
    for (i = 0; i < NUM_DICE; ++i)
    {
        printf(S_DIE_FORM, VB, (EYES(i) > 3) ? S_DIE_EYE_LR : (EYES(i) > 1)
            ? (SLOPE_UP(i) ? S_DIE_EYE_L : S_DIE_EYE_R)
            : S_DIE_EYE_NULL, VB);
    }
    printf("\n");
}

void print_dice(const struct Throw *t)
{
    printf("\nThrow #%d:\n", t->total_throws);
    print_vertical(NUM_DICE, 1);
    print_middle(t);
    print_vertical(NUM_DICE, 0);
    printf("\n> ");
}

void print_help(void)
{
    printf("In Petals Around the Rose I will throw five dice, and it is your objective to\n");
    printf("figure out the result of any given throw. To this end I may tell you three\n");
    printf("things:\n");
    printf("\t1. The name of the game is Petals Around the Rose, and that name is\n");
    printf("\t   significant.\n");
    printf("\t2. The answer to the current throw.\n");
    printf("\t3. The fact that the result is always even.\n\n");
    printf("Commands:\n");
    printf("\tany integer\tInterpreted as a guess.\n");
    printf("\tt\t\t[T]ell the result.\n");
    printf("\thelp\t\tShow this screen.\n");
    printf("\texit\t\tExit.\n");
}

int main()
{
    char input[10] = {0};
    char *end;
    long val = 0;
    struct Throw *t = calloc(1, sizeof(struct Throw));

    if (t == NULL)
    {
        printf("Couldn't allocate memory. Terminating...\n");
        goto error;
    }

    srand((unsigned) time(NULL));
    throw_dice(t);

    printf("Welcome to Petals Around the Rose!\n");
    print_help();

    for (;;)
    {
        print_dice(t);
        if (read_line(input, sizeof(input), stdin) == NULL)
        {
            printf("Error getting input. Terminating...\n");
            goto error;
        }
        val = strtol(input, &end, 10);
        
        if (end == input    /* Nothing was converted. */
            || *end != '\0')/* Trailing characters. */
        {
            lowercase(input, strlen(input));
            if (strcmp(input, "t") == 0) {
                printf("The answer is %d.\n", t->result);
                throw_dice(t);
            } else if (strcmp(input, "exit") == 0) {
                break;
            } else if (strcmp(input, "help") == 0) {
                print_help();
            } else {
                printf("Please input an integer only.\n");
            }
        }         
        else
        {
            if (val == t->result)
            {
                printf("That's right!\n");
                ++t->streak;

                if (t->streak == 10) {
                    printf("10-streak! I think you've figured it out by now. Congratulations!\n");
                } else if (t->streak == 5) {
                    printf("5-streak! Did you figure it out?\n");
                }

                throw_dice(t);
            }
            else
            {
                if (val % 2 == 1) {
                    printf("That's not right. Remember, the result is always even.\n");
                } else {
                    printf("That's not right.\n");
                }
                t->streak = 0;
            }
        }

    }

    free(t);

    return 0;

error:
    return 1;
}
