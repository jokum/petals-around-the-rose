#pragma once

#include <stdint.h>
#include <stdio.h>

#define NUM_DICE 5

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

struct Throw {
    uint8_t result;
    uint16_t total_throws;
    uint8_t streak;
    uint8_t dice[NUM_DICE][2];
};

/*  Safely reads one line from FILE *f.
    This fgets wrapper-function reads max len - 1 characters into buf followed
    by NUL. If any more characters remain in the buffer these are discarded.
    Returns fgets' return value.
    See http://home.datacomm.ch/t_wolf/tw/c/getting_input.html
    or  http://www.drpaulcarter.com/cs/common-c-errors.php#4.3
*/
char *read_line(char *buf, const size_t len, FILE *f);

/*  Safely converts str to a lowercase string.
    Also sets the last character to NUL.
*/
void lowercase(char *str, const size_t len);

/*  Performs a dice throw.
    Assigns a random number [1-6] to the dice array, updates total throw count,
    and calculates the result of the throw.
*/
void throw_dice(struct Throw *t);

/*  Prints the vertical border of num dice.
    If top is non-zero, prints the top border. Else, the bottom border.
*/
void print_vertical(const uint8_t num, const uint8_t top);

/*  Prints the faces of the dice in the passed throw.
*/
void print_middle(const struct Throw *t);

/*  Outputs the dice array and the throw number.
*/
void print_dice(const struct Throw *t);
void print_help(void);
