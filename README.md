# Petals Around the Rose

These are implementations of the *Petals Around the Rose* brain teaser in various languages.

*Petals Around the Rose* is a puzzle with [concealed rules][wiki], meaning (some of) the actual rules of the game are deliberately kept from the players, and it is then their objective to discover what those rules are. Consequently, the game also has no real replay value, except possibly to pester innocent acquaintances with it. For this reason, the concealed rules are in the separate file `RULES.md` (with appropriate spoiler warning).

I was first introduced to the game via [Lloyd Borrett's JavaScript implementation][borrett], which I have no idea how I found, back in 2007.

## How to play

The computer will tell you three things:

1. The name of the game is *Petals Around the Rose*, and that name is significant.
2. The answer is always even.
3. The correct answer to any given throw.

It is the player's objective to determine how the correct score is calculated. They may guess as many times as they want on as many throws as they want.

---

# Languages

* **C 89**

    CLI. Compiles on MSVC 2010 and GCC 4.6.3.

* **C++11**

    CLI. Compiles with MSVC 2010 and GCC 4.6.3. Includes NMAKE makefile with commented-out `make` instructions.

* **C# 3.5**

    CLI/WPF GUI.

* **Java 7**

    Swing GUI.

* **JavaScript**

    HTML5 canvas element using drawn primitives. Could benefit from some refactoring. Only tested in Firefox 12.

[wiki]: http://en.wikipedia.org/wiki/List_of_games_with_concealed_rules
[borrett]: http://www.borrett.id.au/computing/petals-j.htm
