"use strict";
var canvas;
var feed;
var history;
var throws = 0;
var input;
var sideLength = 100;
var side_eye_centre = sideLength / 5; // Distance from the die edge.
var colours = {eye: "#000000",
    die: ["#6677ff", "#00B01D", "#FF6075", "#FFBF00"]};
var petals = 0;
var streak = 0;
var hasGuessed = false;

// Guessing when input comes from the text field.
function submitGuess() {
    var x = input.value;
    if (!isInt(x)) {
        feedback("That doesn't look like an integer.");
    } else {
        x = parseInt(x, 10);
        if (!hasGuessed) { addOneClickGuess(x); }
        makeGuess(x);
    }
    return false;
}

// This is an unordered list that contains one li for every valid guess, with
// an onclick event handler such that the player may simply click the value in
// the list rather than type it in the input field. The list is sorted in
// ascending order.
function addOneClickGuess(x) {
    var guessList, e, k, i, li;
    // If input is an even positive or negative number...
    if (Math.abs(x) % 2 !== 1) {
        // ... and if it hasn't already been added...
        if (!document.getElementById("guess" + x)) {
            // ... create a new element.
            guessList = document.getElementById("guessList");
            e = document.createElement("li");
            e.className = "guess-li";
            e.innerHTML = ['<div class="button" id="guess',
                x, '">', x, '</div>'].join('');
            e.addEventListener("click", function() {
                makeGuess(x);
                return false;
            });
            // If this is not the first even guess...
            if (guessList.childNodes.length > 0) {
                // ... run through all the children...
                k = guessList.childNodes.length;
                for (i = 0; i < k; ++i) {
                    // ... and make sure they're actually valid.
                    li = guessList.childNodes[i];
                    if (li.firstChild) {
                        // If they are...
                        if (x < parseInt(li.firstChild.firstChild.data, 10)) {
                            // ... and current guess is less than current
                            // existing element, add the new element above
                            // the existing one, then get out of here.
                            guessList.insertBefore(e, li);
                            break;
                        } else if (i === guessList.childNodes.length - 1) {
                            // Else, the current guess is greater than all
                            // previous guesses; add it last.
                            guessList.appendChild(e);
                        }
                    }
                }
            } else {
                // First guess to be added to the list.
                guessList.insertBefore(e, guessList.firstChild);
            }
        }
    }
}

// Copy the main canvas to the history list. result is the value of the throw,
// guessed is true if the player guessed it, false otherwise.
function copyCanvas(result, guessed) {
    var dst;
    var e;
    var max = 11;
    var x = throws % max;
    if (throws <= max) {
        e = document.createElement("li");
        e.className = "his-li";
        --x;
        e.id = "his-li" + (throws - 1);
    } else {
        e = document.getElementById("his-li" + x);
    }
    guessed = guessed !== "undefined" ? guessed : false;
    e.innerHTML = ['<canvas class="', (guessed ? 'good' : 'bad'),
        ' history-item" id="his', x,
        '" width="200" height="30"></canvas>'].join('');
    history.insertBefore(e, history.firstChild);
    dst = document.getElementById("his" + x).getContext("2d");
    // Write first, then scale. Then, oddly, the text isn't scaled down.
    dst.font = "10pt Arial";
    dst.fillText("= " + result, 170, 20);
    dst.scale(0.30, 0.30);
    dst.drawImage(canvas, 0, 0);
}

// The actual guess handler.
function makeGuess(guess) {
    input.value = "";
    if (hasGuessed) {
        feedback("You need to throw the dice first.");
    } else {
        if (guess === petals) {
            feedback("That's right! The answer is " + petals + ".");
            if (!hasGuessed) {
                ++streak;
                if (streak % 10 === 0) {
                    feedback("10-streak! I think you got it now.");
                } else if (streak % 5 === 0) {
                    feedback("5-streak! Looking good.");
                }
                hasGuessed = true;
                copyCanvas(petals, true);
            }
        } else {
            streak = 0;
            if (Math.abs(guess) % 2 === 1) {
                feedback("The answer is always even.");
            } else {
                feedback("That's not right.");
            }
        }
    }
}

function feedback(string) {
    feed.innerHTML = string;
}

function isInt(val) {
    return (/^[-+]?\d+$/).test(val);
}

// This wrapper is so throwDice() can be called directly.
function doDiceThrow() {
    if (!hasGuessed) {
        feedback("At least make an effort!");
    } else {
        throwDice();
    }
}

function tellMe() {
    feedback("The answer is " + petals + ".");
    if (!hasGuessed) {
        hasGuessed = true;
        streak = 0;
        copyCanvas(petals, false);
    }
}

function throwDice() {
    var eyes = 0;
    hasGuessed = false;
    petals = 0;
    ++throws;
    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    for (var i = 1; i <= 5; ++i) {
        eyes = Math.floor(Math.random() * 6) + 1;
        drawDie(i, eyes);
        petals += (eyes % 2 === 1) ? eyes - 1 : 0;
    }
    return false;
}

// Draw die number n at position x0.
function drawDie(x0, n) {
    var x = (x0 - 1) * (sideLength + 10);
    roundedRect(canvas.getContext("2d"), x, 0, sideLength, 10);
    drawFace(x, n);
}

// Draw face with val eyes at position x0.
function drawFace(x0, val) {
    switch (val) {
        case 1:
            drawOne(x0);
            break;
        case 2:
            drawTwo(x0, Math.floor(Math.random() * 2));
            break;
        case 3:
            drawOne(x0);
            drawTwo(x0, Math.floor(Math.random() * 2));
            break;
        case 4:
            drawFour(x0);
            break;
        case 5:
            drawOne(x0);
            drawFour(x0);
            break;
        case 6:
            drawFour(x0);
            drawTwoMid(x0);
            break;
        default:
            console.log("Invalid face value: " + x0);
    }
}

function drawOne(x0) {
    var x = x0 + sideLength / 2;
    var y = x - x0;
    drawEye(x, y);
}

// Draw two corner eyes, their position decided by slopeUp.
function drawTwo(x0, slopeUp) {
    slopeUp = typeof slopeUp !== "undefined" ? true : false;
    var x = x0 + side_eye_centre;
    var y = x - x0;

    if (slopeUp) {
        y = sideLength - y;
    }
    drawEye(x, y);

    x = x0 + sideLength - side_eye_centre;

    if (slopeUp) {
        y = sideLength - y;
    } else {
        y = x - x0;
    }
    drawEye(x, y);
}

function drawFour(x0) {
    drawTwo(x0);
    drawTwo(x0, true);
}

function drawTwoMid(x0) {
    var x = x0 + side_eye_centre;
    var y = sideLength / 2;
    drawEye(x, y);

    x = x0 + sideLength - side_eye_centre;
    drawEye(x, y);
}

function drawEye(x, y) {
    var ctx = canvas.getContext("2d");
    var radius = 10;
    var startAngle = 0;
    var endAngle = Math.PI * 2;

    ctx.fillStyle = colours.eye;
    ctx.beginPath();
    ctx.arc(x, y, radius, startAngle, endAngle, true);
    ctx.fill();
}

function roundedRect(ctx, x, y, sideLength, radius) {
    ctx.fillStyle = colours.die[
        Math.floor(Math.random() * colours.die.length)];
    ctx.beginPath();
    ctx.moveTo(x, y + radius);
    // Left.
    ctx.lineTo(x, y + sideLength - radius);
    ctx.quadraticCurveTo(x, y + sideLength, x + radius, y + sideLength);
    // Bottom.
    ctx.lineTo(x + sideLength - radius, y + sideLength);
    ctx.quadraticCurveTo(x + sideLength, y + sideLength, x + sideLength, y + sideLength - radius);
    // Right.
    ctx.lineTo(x + sideLength, y + radius);
    ctx.quadraticCurveTo(x + sideLength, y, x + sideLength - radius, y);
    // Top.
    ctx.lineTo(x + radius, y);
    ctx.quadraticCurveTo(x, y, x, y + radius);
    ctx.fill();
}

function keydown(e) {
    // Activate input on 0-9, NUM 0-9, or - (minus).
    if (e.keyCode >= 48 && e.keyCode <= 57  ||
            e.keyCode >= 96 && e.keyCode <= 105 ||
            e.keyCode == 109) {
        input.focus();
    } else if (e.keyCode === 13) { // Enter.
        if (document.activeElement === input) {
            input.blur();
            submitGuess();
        }
    } else if (e.keyCode === 84) { // [T]ell me.
        if (document.activeElement === input) {
            input.value = input.value.substr(0, input.value.length - 1);
            input.blur();
        }
        tellMe();
    } else if (e.keyCode === 68) { // [D]ice, throw.
        if (document.activeElement === input) {
            input.value = input.value.substr(0, input.value.length - 1);
            input.blur();
        }
        doDiceThrow();
    }
}

function start() {
    canvas = document.getElementById("diceCanvas");
    if (!canvas.getContext) {
        return;
    }
    feed = document.getElementById("status");
    history = document.getElementById("history");
    input = document.getElementById("number");
    input.onfocus = function() { this.value = ""; };
    throwDice();
    document.getElementById("guess").onclick = submitGuess;
    document.getElementById("throwDice").onclick = doDiceThrow;
    document.getElementById("tellMe").onclick = tellMe;
    document.onkeydown = keydown;
}
