﻿using System;

namespace PAtR
{
    public class Rose
    {
        public byte[,] Dice
        {
            get { return (byte[,]) dice.Clone(); }
        }

        public void Throw()
        {
            result = 0;
            for (int i = 0; i < dice.GetLength(0); ++i)
            {
                dice[i, 0] = (byte)rng.Next(1, 7);
                dice[i, 1] = (byte)rng.Next(0, 2);
                result += (byte)((dice[i, 0] % 2 == 1) ? dice[i, 0] - 1 : 0);
            }
            ++TotalThrows;
        }

        public UInt16 TotalThrows
        {
            get; private set;
        }

        public bool Guess(byte guess)
        {
            if (guess == result)
            {
                ++Streak;
                return true;
            }
            Streak = 0;
            return false;
        }

        public UInt16 Streak
        {
            get; private set;
        }

        public byte Tell()
        {
            Streak = 0;
            return result;
        }

        private byte result;
        private byte[,] dice = new Byte[5, 2];
        private static Random rng = new Random();
    }
}
