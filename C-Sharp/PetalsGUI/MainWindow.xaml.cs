﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using PAtR;

namespace PetalsGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Rose controller = new Rose();
        private BitmapImage[,] _imageSources = new BitmapImage[6, 2];
        private Image[] _images;

        public MainWindow()
        {
            InitializeComponent();

            btnThrow.Focus();
            _images = new Image[] { image1, image2, image3, image4, image5 };
            for (int eye = 0; eye < _imageSources.GetLength(0); ++eye)
            {
                for (int slope = 0; slope < 2; ++slope)
                {
                    _imageSources[eye, slope] = App.Current.Resources["die" + eye + slope] as BitmapImage;
                }
            }
        }

        private void btnThrow_Click(object sender, RoutedEventArgs e)
        {
            controller.Throw();
            byte[,] dice = controller.Dice;
            for (byte eye = 0; eye < dice.GetLength(0); ++eye)
            {
                _images[eye].Source = _imageSources[dice[eye, 0] - 1, dice[eye, 1]];
            }
            btnTell.IsEnabled = true;
            btnThrow.IsEnabled = false;
            btnGuess.IsEnabled = true;
            txtGuess.Focus();
        }

        private void btnGuess_Click(object sender, RoutedEventArgs e)
        {
            byte guess;
            if (Byte.TryParse(txtGuess.Text, out guess))
            {
                if (controller.Guess(guess))
                {
                    if (controller.Streak == 10)
                    {
                        Inform("10-streak! I think you've figured it out by now.");
                    }
                    else if (controller.Streak == 5)
                    {
                        Inform("5-streak! Did you figure it out?");
                    }
                    else
                    {
                        Inform("That's right!");
                    }
                    btnTell.IsEnabled = false;
                    btnThrow.IsEnabled = true;
                    btnGuess.IsEnabled = false;
                    btnThrow.Focus();
                }
                else
                {
                    if (guess % 2 == 1)
                    {
                        Inform("That's not right. The result is always even.");
                    }
                    else
                    {
                        Inform("That's not right.");
                    }
                }
            }
            else
            {
                Inform("I don't know what that means.");
            }
        }

        private void Inform(string info)
        {
            lblInfo.Content = info;
        }

        private void btnTell_Click(object sender, RoutedEventArgs e)
        {
            Inform("The answer is " + controller.Tell() + ".");
            btnThrow.IsEnabled = true;
            btnGuess.IsEnabled = false;
        }

        private void txtGuess_GotFocus(object sender, RoutedEventArgs e)
        {
            txtGuess.SelectAll();
        }

        private void txtGuess_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnGuess_Click(sender, e);
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }
    }
}
