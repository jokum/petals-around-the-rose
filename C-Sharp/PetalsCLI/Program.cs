﻿using PAtR;

namespace PetalsCLI
{
    class Program
    {
        static void Main()
        {
            new PetalsCLI(new Rose()).Play();
        }
    }
}
