﻿using System;
using PAtR;

namespace PetalsCLI
{
    public class PetalsCLI
    {
        private Rose controller;

        public PetalsCLI(Rose r)
        {
            controller = r;
        }

        public void Play()
        {
            string input;
            byte guess;
            controller.Throw();
            Console.WriteLine("Welcome to Petals Around the Rose!");
            PrintHelp();
            while (true)
            {
                PrintDice();
                input = Console.ReadLine().ToLower();
                if (Byte.TryParse(input, out guess))
                {
                    MakeGuess(guess);
                }
                else if (input.Equals("t"))
                {
                    Tell();
                }
                else if (input.Equals("help"))
                {
                    PrintHelp();
                }
                else if (input.Equals("exit"))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Unrecognized command: {0}. Type help to see available commands.", input);
                }
            }
        }

        private void MakeGuess(byte guess)
        {
            if (controller.Guess(guess))
            {
                if (controller.Streak == 10)
                {
                    Console.WriteLine("10-streak! I think you've figured it out by now.");
                }
                else if (controller.Streak == 5)
                {
                    Console.WriteLine("5-streak! Did you figure it out?");
                }
                else
                {
                    Console.WriteLine("That's right!");
                }
                controller.Throw();
            }
            else
            {
                if (guess % 2 == 1)
                {
                    Console.WriteLine("That's not right. Remember, the result is always even.");
                }
                else
                {
                    Console.WriteLine("That's not right.");
                }
            }
        }

        private void Tell()
        {
            Console.WriteLine("The result is {0}.", controller.Tell());
            controller.Throw();
        }

        private void PrintHelp()
        {
            Console.WriteLine("In Petals Around the Rose I will throw five dice, and it is your objective to");
            Console.WriteLine("figure out the result of any given throw. To this end I may tell you three");
            Console.WriteLine("things:");
            Console.WriteLine("\t1. The name of the game is Petals Around the Rose, and that name is");
            Console.WriteLine("\t   significant.");
            Console.WriteLine("\t2. The answer to the current throw.");
            Console.WriteLine("\t3. The fact that the result is always even.\n");
            Console.WriteLine("Commands:");
            Console.WriteLine("\tany integer\tInterpreted as a guess.");
            Console.WriteLine("\tt\t\t[T]ell the result.");
            Console.WriteLine("\thelp\t\tShow this screen.");
            Console.WriteLine("\texit\t\tExit.");
        }

        private void PrintDice()
        {
            Console.WriteLine("{0}Throw #{1}:", Console.Out.NewLine, controller.TotalThrows);
            byte[,] b = controller.Dice;

            for (byte i = 0; i < b.GetLength(0); ++i)
            {
                Console.Write("--------- ");
            }
            Console.WriteLine();

            for (byte i = 0; i < b.GetLength(0); ++i)
            {
                Console.Write("| {0} | ", (b[i, 0] > 3) ? "o   o" : (b[i, 0] > 1)
                    ? (b[i, 1] == 1 ? "    o" : "o    ") // True => 2,3 slope up.
                    : "     ");
            }
            Console.WriteLine();
            for (byte i = 0; i < b.GetLength(0); ++i)
            {
                Console.Write("| {0} | ", (b[i, 0] % 2 == 1)
                    ? "  o  " : (b[i, 0] == 6) ? "o   o" : "     ");
            }
            Console.WriteLine();
            for (byte i = 0; i < b.GetLength(0); ++i)
            {
                Console.Write("| {0} | ", (b[i, 0] > 3) ? "o   o" : (b[i, 0] > 1)
                    ? (b[i, 1] == 1 ? "o    " : "    o") // True => 2,3 slope up.
                    : "     ");
            }
            Console.WriteLine();

            for (byte i = 0; i < b.GetLength(0); ++i)
            {
                Console.Write("--------- ");
            }
            Console.WriteLine();
            
            Console.Write("{0}> ", Console.Out.NewLine);
        }
    }
}
